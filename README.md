[Symbol_Game]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Sprites/Other/IJWLC0
[Symbol_Important]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Graphics/IPOGFORW


# IPOG Classes - Heretic Patch

### ![Symbol_Important] IMPORTANT: This requires [IPOG Classes](https://gitlab.com/ipog-mod/ipogclasses) to work!

## ![Symbol_Game]**Description**

This patch allows the mod "IPOG Classes" to work on Heretic.


## ![Symbol_Game]**Compatibility**

Classic Doom source ports supporting ACS and DECORATE: GZDoom 4.2.4 or above (except 4.3 ones), LZDoom 3.84 or above (except 3.85). It might work on older versions.


## ![Symbol_Game]Licensing

In Pursuit of Greed Classes - Heretic Patch project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html). The artwork has a [custom license](https://archive.org/details/GreedSource):

"These sources are provided for educational and historical purposes. No assets or code may be used in any way commercially. Personal and educational use only."
